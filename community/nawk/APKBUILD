# Maintainer: John Vogel <jvogel4@stny.rr.com>
pkgname=nawk
pkgver=20240422_git20240623
_commit=0fb991ab7e957d1a464cbdb960a28cec942484b8
_ckver=20240422
pkgrel=0
pkgdesc="The one, true implementation of AWK"
url="https://github.com/onetrueawk/awk/"
arch="all"
license=MIT
makedepends="bison"
[ -z "$BOOTSTRAP" ] && checkdepends="cmd:pr musl-locales"
[ -n "$BOOTSTRAP" ] && options="$options !check"
subpackages="$pkgname-doc"
source="awk-$_commit.tar.gz::https://github.com/onetrueawk/awk/archive/$_commit.tar.gz
	awk-$_ckver.tar.gz::https://github.com/onetrueawk/awk/archive/refs/tags/$_ckver.tar.gz"
builddir="$srcdir/awk-$_commit"

build() {
	make
}

check() {
	make -C "$srcdir"/awk-"$_ckver"
	oldawk="$srcdir"/awk-"$_ckver"/a.out make check
}

package() {
	install -Dm755 a.out "$pkgdir"/usr/bin/$pkgname
	install -Dm644 awk.1 "$pkgdir"/usr/share/man/man1/$pkgname.1
	install -Dm644 FIXES FIXES.1e LICENSE README.md TODO \
		-t "$pkgdir"/usr/share/doc/"$pkgname"
}

sha512sums="
364440c73fc5b0b11fdbba30abbaeabf9cb3c84b95424d96f9b254b7461c17a70b6b1d3c2344c074a63499feb109945973e8f586aecb5bb0381e98b77f7bdec2  awk-0fb991ab7e957d1a464cbdb960a28cec942484b8.tar.gz
3d5626b0d6033a9eb8f3d81acbe0b30842e99f4c40563ca3f906ecfebdb72d754de18cd180345131868ad325fead4a6e66bfb765af45267180e256f46a76f316  awk-20240422.tar.gz
"

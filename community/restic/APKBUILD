# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: Sören Tempel <soeren+alpine@soeren-tempel.net>
pkgname=restic
pkgver=0.16.5
pkgrel=1
pkgdesc="Fast, secure, efficient backup program"
url="https://restic.net/"
arch="all"
license="BSD-2-Clause"
makedepends="go"
options="net chmod-clean"
subpackages="$pkgname-doc $pkgname-bash-completion $pkgname-zsh-completion"
source="https://github.com/restic/restic/releases/download/v$pkgver/restic-$pkgver.tar.gz"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -v ./cmd/...
}

check() {
	RESTIC_TEST_FUSE=0 make test
}

package() {
	install -Dm755 $pkgname "$pkgdir"/usr/bin/$pkgname

	local man
	for man in doc/man/*.?; do
		install -Dm644 "$man" \
			"$pkgdir"/usr/share/man/man${man##*.}/${man##*/}
	done

	install -Dm644 doc/bash-completion.sh \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 doc/zsh-completion.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
57a1829e28c4317172c06e095d91d760466c589cc9d2f2a5f20491ffe60f7a7c60e4666377fa5ae275b3238c10c27ab9265ea4f9f98c34b288fbd1e70a0814c6  restic-0.16.5.tar.gz
"

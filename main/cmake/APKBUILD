# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=cmake
pkgver=3.30.0
pkgrel=0
pkgdesc="Cross-platform, open-source make system"
url="https://www.cmake.org/"
arch="all"
license="BSD-3-Clause"
makedepends="
	bzip2-dev
	expat-dev
	libarchive-dev
	libuv-dev
	linux-headers
	ncurses-dev
	py3-sphinx
	rhash-dev
	samurai
	xz-dev
	zlib-dev
	"
checkdepends="file"
subpackages="
	ccmake
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-emacs::noarch
	$pkgname-vim::noarch
	"
case $pkgver in
*.*.*.*) _v=v${pkgver%.*.*};;
*.*.*) _v=v${pkgver%.*};;
esac
source="https://www.cmake.org/files/$_v/cmake-$pkgver.tar.gz"
options="!check"

build() {
	# jsoncpp/cppdap/curl/nghttp2 needs cmake to build so to avoid recursive build
	# dependency, we use the bundled version.
	# Do NOT remove --no-system-*

	./bootstrap \
		--prefix=/usr \
		--mandir=/share/man \
		--datadir=/share/$pkgname \
		--docdir=/share/doc/$pkgname \
		--sphinx-man \
		--no-system-cppdap \
		--no-system-curl \
		--no-system-jsoncpp \
		--no-system-nghttp2 \
		--system-bzip2 \
		--system-expat \
		--system-libarchive \
		--system-liblzma \
		--system-librhash \
		--system-libuv \
		--system-zlib \
		--generator=Ninja \
		--parallel="${JOBS:-2}"
	ninja
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE bin/ctest
}

package() {
	DESTDIR="$pkgdir" ninja install
}

ccmake() {
	pkgdesc="$pkgdesc (ccmake configurator)"

	amove usr/bin/ccmake
}

emacs() {
	pkgdesc="$pkgdesc (emacs mode)"
	install_if="$pkgname=$pkgver-r$pkgrel emacs"

	amove usr/share/emacs
}

vim() {
	pkgdesc="$pkgdesc (vim files)"
	install_if="$pkgname=$pkgver-r$pkgrel vim"

	amove usr/share/vim
}

sha512sums="
1dd872a3f93cfadb7ac0f93177e7d4d7ded67cf09c71b1b776dc3bc8b7d4e44dd81533a8a4bf2c9eaaed56443060aa27a11250a5e47de316c89db7e146929782  cmake-3.30.0.tar.gz
"
